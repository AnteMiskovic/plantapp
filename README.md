# Plant App - MAP PROJEKT - Ante Mišković

Aplikacija će služiti kao mala pomoć mojoj djevojci koja se bavi opskrbom svojih klijenata raznim vrstama biljaka.

Svrha jest 
1. održavati bazu svojih klijenata i pohranjivati najnužnije informacije o svakome od njih.
2. vezivati uz klijenta biljke koje je dotičnom nužno isporučiti. 
3. održavati bazu biljaka i njihovih cijena.

## BAZA
SqLite 

- [Insert Skripta - biljke](https://gitlab.com/AnteMiskovic/plantapp/-/blob/main/app/src/main/res/raw/Plants.sql)
- [Insert Skripta - klijenti](https://gitlab.com/AnteMiskovic/plantapp/-/blob/main/app/src/main/res/raw/Clients.sql)

