package hr.vsite.map.plantapp;

import android.provider.BaseColumns;

public class DBContract {
    private DBContract(){}
    public final static String DATABASE_NAME = "PlantApp.db";

    public static abstract class Plant implements BaseColumns {
        public static String TABLE_NAME   = "Plants";
        public static String COLUMN_NAME   = "Name";
        public static String COLUMN_COMMONNAME   = "CommonName";
        public static String COLUMN_FAMILY   = "Family";
        public static String COLUMN_PRICE   = "Price";

        public static String CREATE_TABLE_SCRIPT =
                " CREATE TABLE " + TABLE_NAME + " ( " +
                        "Id " + " INTEGER PRIMARY KEY AUTOINCREMENT,"  +
                        COLUMN_NAME + " TEXT NOT NULL, " +
                        COLUMN_COMMONNAME + " TEXT NOT NULL, " +
                        COLUMN_FAMILY + " TEXT NOT NULL, " +
                        COLUMN_PRICE + " TEXT NOT NULL " +
                        ")";
    }


    public static abstract class Client implements BaseColumns {
        public static String TABLE_NAME   = "Clients";
        public static String COLUMN_NAME   = "Name";
        public static String COLUMN_ADDRESS   = "Address";
        public static String COLUMN_PHONENUMBER  = "PhoneNumber";

        public static String CREATE_TABLE_SCRIPT =
                " CREATE TABLE " + TABLE_NAME + " ( " +
                        "Id " + " INTEGER PRIMARY KEY AUTOINCREMENT,"  +
                        COLUMN_NAME + " TEXT NOT NULL, " +
                        COLUMN_ADDRESS + " TEXT NOT NULL, " +
                        COLUMN_PHONENUMBER + " TEXT NOT NULL " +
                        ")";
    }

    public static abstract class ClientPlant implements BaseColumns {
        public static String TABLE_NAME   = "ClientsPlants";
        public static String COLUMN_AMOUNT   = "Amount";
        public static String COLUMN_STATUS   = "Status";
        public static String COLUMN_REMARK   = "Remark";
        public static String COLUMN_CLIENTID  = "ClientId";
        public static String COLUMN_PLANTID  = "PlantId";

        public static String CREATE_TABLE_SCRIPT =
                " CREATE TABLE " + TABLE_NAME + " ( " +
                        "Id " + " INTEGER PRIMARY KEY autoincrement,"  +
                        COLUMN_AMOUNT + " INT NOT NULL, " +
                        COLUMN_STATUS + " INT NOT NULL, " +
                        COLUMN_REMARK + " TEXT NULL, " +
                        COLUMN_CLIENTID + " INT NOT NULL, " +
                        COLUMN_PLANTID + " INT NOT NULL, " +
                        "FOREIGN KEY(ClientId)" + " REFERENCES  Clients(Id)," +
                        "FOREIGN KEY(PlantId)" + " REFERENCES  Plants(Id)" +
                        ")";
    }
}
