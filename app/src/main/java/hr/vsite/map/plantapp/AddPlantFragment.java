package hr.vsite.map.plantapp;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

public class AddPlantFragment extends Fragment {
    EditText eName;
    EditText eCommon;
    EditText eFamily;
    EditText ePrice;
    Button btnAdd;

    public interface AddPlantFragmentListener{
        void onInputPlantFragmentSent(PlantModel input);
    }
    private AddPlantFragmentListener listener;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.fragment_add_plant, container, false);
        btnAdd = view.findViewById(R.id.btnAddNewPlant);
        eName = view.findViewById(R.id.etCreatePlantName);
        eCommon = view.findViewById(R.id.etCreatePlantCommonName);
        eFamily = view.findViewById(R.id.etCreatePlantFamily);
        ePrice = view.findViewById(R.id.etCreatePlantPrice);
        btnAdd.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){
                PlantModel model = new PlantModel(-1, eName.getText().toString(),eCommon.getText().toString(),eFamily.getText().toString(), Double.parseDouble(ePrice.getText().toString()));
                        listener.onInputPlantFragmentSent(model);
            }
        });
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof  AddPlantFragmentListener){
            listener = (AddPlantFragmentListener) context;
        }
        else
            {
            throw new RuntimeException(context.toString() + " implementiraj FragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
}