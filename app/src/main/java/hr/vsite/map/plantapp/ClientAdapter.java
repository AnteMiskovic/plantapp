package hr.vsite.map.plantapp;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.ArrayList;

public class ClientAdapter extends ArrayAdapter<ClientModel> {

private ArrayList<ClientModel> clientList;

    public ClientAdapter(@NonNull Context context, int resource, ArrayList<ClientModel> clientList) {
        super(context, resource, clientList);
        this.clientList = clientList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_client,parent,false);
        }
        TextView tv_name = convertView.findViewById(R.id.client_name_textview);
        TextView tv_address = convertView.findViewById(R.id.client_address_textview);

        tv_name.setText(clientList.get(position).Name);
        tv_address.setText(clientList.get(position).Address);
        return convertView;
    }
}
