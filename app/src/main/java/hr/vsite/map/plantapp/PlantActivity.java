package hr.vsite.map.plantapp;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.List;

public class PlantActivity extends AppCompatActivity {
    List<PlantModel> list = new ArrayList<PlantModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plant);
        Intent intent = this.getIntent();
        if( intent != null)
            list = (List<PlantModel>) intent.getSerializableExtra("list");

        ArrayList<PlantModel> arrList = new ArrayList<PlantModel>();
        arrList.addAll(list);

        PlantAdapter adapter = new PlantAdapter(getApplicationContext(),R.layout.list_item,arrList);
        ListView listview =  findViewById(R.id.plant_list);
        listview.setAdapter(adapter);
    }
}