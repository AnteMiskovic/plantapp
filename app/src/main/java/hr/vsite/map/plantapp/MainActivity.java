package hr.vsite.map.plantapp;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import java.io.Serializable;

public class MainActivity extends AppCompatActivity implements AddFragment.AddFragmentListener, AddPlantFragment.AddPlantFragmentListener {

    DBHelper helper = new DBHelper(this);
    private AddFragment addClientFragment;
    private AddPlantFragment addPlantFragment;
    Button btnPlant;
    Button btnClient;
    Button btnPlantAdd;
    Button btnPlantShow;
    Button btnClientAdd;
    Button btnClientShow;
    boolean clientFlag = false;
    boolean plantFlag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setButtons();
    }

    public void onClick(View v){
        Intent i;
        switch(v.getId())
        {
                case R.id.btnPlant:
                    if( !plantFlag ) {
                        btnPlantAdd.setVisibility(Button.VISIBLE);
                        btnPlantShow.setVisibility(Button.VISIBLE);
                        plantFlag = !plantFlag;
                        return;
                    }
                    if(plantFlag){
                        btnPlantAdd.setVisibility(Button.INVISIBLE);
                        btnPlantShow.setVisibility(Button.INVISIBLE);
                        plantFlag = !plantFlag;
                        return;
                    }
                case R.id.btnClient:
                    if( !clientFlag ) {
                        btnClientAdd.setVisibility(Button.VISIBLE);
                        btnClientShow.setVisibility(Button.VISIBLE);
                        clientFlag = !clientFlag;
                        return;
                    }
                    if(clientFlag){
                        btnClientAdd.setVisibility(Button.INVISIBLE);
                        btnClientShow.setVisibility(Button.INVISIBLE);
                        clientFlag = !clientFlag;
                        return;
                    }

                case R.id.btn_plant_viewall:
                    i = new Intent(MainActivity.this, PlantActivity.class);
                    i.putExtra("list",(Serializable) helper.GetPlants());
                    startActivity(i);
                    btnPlant.callOnClick();
                    break;
                case R.id.btn_plant_add:
                    addPlantFragment = new AddPlantFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.container , addPlantFragment).commit();
                    btnPlant.callOnClick();
                    break;
                case R.id.btn_client_add:
                    addClientFragment = new AddFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.container , addClientFragment).commit();
                    btnClient.callOnClick();
                      break;
                case R.id.btn_client_viewall:
                    i = new Intent(MainActivity.this, ClientActivity.class);
                    i.putExtra("list",(Serializable) helper.GetClients());
                    startActivity(i);
                    btnClient.callOnClick();
                    break;

                default:
                break;
        }
    }

    private void setButtons() {
        btnClient = findViewById(R.id.btnClient);
        btnPlant = findViewById(R.id.btnPlant);
        btnPlantAdd = findViewById(R.id.btn_plant_add);
        btnPlantShow = findViewById(R.id.btn_plant_viewall);
        btnClientAdd = findViewById(R.id.btn_client_add);
        btnClientShow = findViewById(R.id.btn_client_viewall);
    }

    @Override
    public void onInputFragmentSent(ClientModel input) {
        helper.AddClient(input);
        Toast.makeText(MainActivity.this, R.string.clientAdded, Toast.LENGTH_LONG).show();
        getSupportFragmentManager().beginTransaction().hide(addClientFragment).commit();
    }

    @Override
    public void onInputPlantFragmentSent(PlantModel input) {
        helper.AddPlant(input);
        Toast.makeText(MainActivity.this, R.string.plantAdded, Toast.LENGTH_LONG).show();
        getSupportFragmentManager().beginTransaction().hide(addPlantFragment).commit();
    }
}