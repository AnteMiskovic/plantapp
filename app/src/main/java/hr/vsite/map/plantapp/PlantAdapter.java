package hr.vsite.map.plantapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class PlantAdapter extends ArrayAdapter<PlantModel> {
    private ArrayList<PlantModel> plantList;

    public PlantAdapter(@NonNull Context context, int resource, ArrayList<PlantModel> plantList) {
        super(context, resource, plantList);
        this.plantList = plantList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item,parent,false);
        }

        ImageView plantImage = convertView.findViewById(R.id.plant_imageview);
        TextView tv_name = convertView.findViewById(R.id.name_textview);
        TextView tv_family = convertView.findViewById(R.id.family_textview);

        tv_name.setText(plantList.get(position).Name);
        tv_family.setText(plantList.get(position).Family);
        return convertView;
    }
}
