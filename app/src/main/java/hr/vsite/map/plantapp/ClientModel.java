package hr.vsite.map.plantapp;

import java.io.Serializable;

public class ClientModel implements Serializable {
    public int Id;
    public String Name;
    public String Address;
    public String PhoneNumber;

    public ClientModel() {}
    public ClientModel(int id, String name, String address, String phoneNumber) {
        Id = id;
        Name = name;
        Address = address;
        PhoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return Id + " " + Name;
    }


}
