package hr.vsite.map.plantapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ClientActivity extends AppCompatActivity {
    List<ClientModel> list = new ArrayList<ClientModel>();
    private ClientAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client);
        ListView listview =  findViewById(R.id.client_list);
        Intent intent = this.getIntent();

        if( intent != null)
            list = (List<ClientModel>) intent.getSerializableExtra("list");

        ArrayList<ClientModel> arrList = new ArrayList<ClientModel>();
        arrList.addAll(list);

        adapter = new ClientAdapter(getApplicationContext(), R.layout.list_item, arrList);

        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parentAdapter, View view,
                                    int position, long id) {
                Intent i = new Intent(ClientActivity.this, ClientInteractionActivity.class);
                ClientModel client = list.get(position);
                i.putExtra("client",(Serializable) client);
                startActivity(i);
            }
        });
    }
}