package hr.vsite.map.plantapp;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.List;

public class ClientInteractionActivity extends AppCompatActivity {
    DBHelper helper = new DBHelper(this);
    ArrayAdapter<PlantModel> adapter;
    PlantModel plant;
    ClientModel client;
    EditText etName;
    EditText etAddress;
    EditText etPhone;
    EditText etAmount;
    EditText etRemark;
    Button btnEdit;
    Button btnAddPlantToUser;
    Spinner dropdown;
    TextView tvPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_interaction);
        Intent intent = this.getIntent();

        if( intent != null)
            client = (ClientModel) intent.getSerializableExtra("client");

        setButtons();
        setSpinner();
        setViews();

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editUser();
            }
        });

        btnAddPlantToUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPlantToClient();
            }
        });

        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                 plant = (PlantModel) adapter.getItem(position);
                 float f = Float.valueOf(plant.Price.toString());
                 tvPrice.setText( String.format("%.02f",f));
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {}
        });
    }

    private void addPlantToClient() {
        String remarks = etRemark.getText().toString();
        Integer amount;

        if(etAmount.getText().toString() != "")
            amount = Integer.parseInt(etAmount.getText().toString());
        else {
            Toast.makeText(ClientInteractionActivity.this, R.string.amountNotSpecified, Toast.LENGTH_LONG).show();
            return;
        }

        helper.AddPlantToClient(client.Id,plant.Id,amount,remarks);
        Toast.makeText(ClientInteractionActivity.this, "Added " + amount + " " + plant.CommonName + " to " + client.Name, Toast.LENGTH_LONG).show();
    }
    private void editUser(){

        client.Name = etName.getText().toString();
        client.Address = etAddress.getText().toString();
        client.PhoneNumber = etPhone.getText().toString();

        helper.EditClient(client);
        Toast.makeText(ClientInteractionActivity.this, R.string.edit_user_success, Toast.LENGTH_LONG).show();
        setViews();

    }
    private void setViews() {
        etName.setText(client.Name);
        etAddress.setText(client.Address);
        etPhone.setText(client.PhoneNumber);
    }
    private void setSpinner() {
        List<PlantModel> plants =  helper.GetPlants();
        adapter = new ArrayAdapter<PlantModel>(this, android.R.layout.simple_spinner_item, plants);
        dropdown.setAdapter(adapter);
    }
    private void setButtons() {
        tvPrice = findViewById(R.id.tv_price);
        etName = findViewById(R.id.etName);
        etAddress = findViewById(R.id.etAddress);
        etPhone = findViewById(R.id.etPhone);
        etRemark = findViewById(R.id.et_remark);
        etAmount = findViewById(R.id.et_amount);
        btnEdit = findViewById(R.id.btn_edit_client);
        btnAddPlantToUser = findViewById(R.id.btnAddPlantToClient);
        dropdown = findViewById(R.id.plant_spinner);
    }

}