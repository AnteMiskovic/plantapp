package hr.vsite.map.plantapp;

import java.io.Serializable;

public class PlantModel implements Serializable {
    public int Id;
    public String Name;
    public String CommonName;
    public String Family;
    public Double Price;

    public PlantModel() {}
    public PlantModel(int id, String name, String commonName, String family, Double price) {
        Id = id;
        Name = name;
        CommonName = commonName;
        Family = family;
        Price = price;
    }

    @Override
    public String toString() {
        return CommonName;
    }

}
