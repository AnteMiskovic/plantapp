package hr.vsite.map.plantapp;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddFragment extends Fragment {
    EditText eName;
    EditText eAddr;
    EditText ePhone;
    Button btnAdd;

    public interface AddFragmentListener{
        void onInputFragmentSent(ClientModel input);
    }
    private AddFragmentListener listener;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.fragment_add, container, false);
        btnAdd = view.findViewById(R.id.btnAddNewClient);
        eName = view.findViewById(R.id.etCreateName);
        eAddr = view.findViewById(R.id.etCreateAddress);
        ePhone = view.findViewById(R.id.etCreatePhone);
        btnAdd.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){
                ClientModel c = new ClientModel(-1, eName.getText().toString(),eAddr.getText().toString(),ePhone.getText().toString());
                        listener.onInputFragmentSent(c);
            }
        });
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof  AddFragmentListener){
            listener = (AddFragmentListener) context;
        }
        else
            {
            throw new RuntimeException(context.toString() + " implementiraj FragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
}