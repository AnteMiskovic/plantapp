package hr.vsite.map.plantapp;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.util.ArrayList;
import java.util.List;


import hr.vsite.map.plantapp.DBContract.Plant;
import hr.vsite.map.plantapp.DBContract.Client;
import hr.vsite.map.plantapp.DBContract.ClientPlant;

public class DBHelper extends SQLiteOpenHelper {

    final static int DATABASE_VERSION = 1;
    private static Context _context;

    public DBHelper(@Nullable Context context){
        super(context, DBContract.DATABASE_NAME,null, DATABASE_VERSION);
        _context = context;
        }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Plant.CREATE_TABLE_SCRIPT);
        db.execSQL(Client.CREATE_TABLE_SCRIPT);
        db.execSQL(ClientPlant.CREATE_TABLE_SCRIPT);
        try {
            insertFromFile(db, R.raw.plants);
            insertFromFile(db,R.raw.clients);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(" DROP IF TABLE EXISTS " + Plant.TABLE_NAME);
        db.execSQL(" DROP IF TABLE EXISTS " + Client.TABLE_NAME);
        db.execSQL(" DROP IF TABLE EXISTS " + ClientPlant.TABLE_NAME);
        }

    private void insertFromFile(SQLiteDatabase db,int resourceId) throws IOException{
        InputStream insertsStream = _context.getResources().openRawResource(resourceId);
        BufferedReader insertReader = new BufferedReader(new InputStreamReader(insertsStream));

        String insertStmt = "";
        while (insertReader.ready()) {
            insertStmt = insertReader.readLine();
            db.execSQL(insertStmt);
            insertStmt = "";
        }
        insertReader.close();
    }


    // -----------------******--HELPER METHODS --******---------------------//


    // -------------------------PLANTS----------------------//

    public boolean AddPlant(PlantModel model){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(Plant.COLUMN_NAME, model.Name);
        cv.put(Plant.COLUMN_COMMONNAME, model.CommonName);
        cv.put(Plant.COLUMN_FAMILY, model.Family);

        long insert = db.insert(Plant.TABLE_NAME, null ,cv);
        return insert >= 0 ? true : false;
    }
    public List<PlantModel> GetPlants(){
        List<PlantModel> returnList = new ArrayList<PlantModel>();
        String q = "SELECT * FROM " + Plant.TABLE_NAME + " ORDER BY Name ";
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(q,null);
        if ( cursor.moveToFirst()){

            do {
                PlantModel plant = new PlantModel();
                plant.Id = cursor.getInt(0);
                plant.Name = cursor.getString(1);
                plant.CommonName = cursor.getString(2);
                plant.Family = cursor.getString(3);
                plant.Price = cursor.getDouble(4);
                returnList.add(plant);
            } while(cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return returnList;
    }
    public boolean AddPlantToClient(int clientId,int plantId,int amount, String remarks){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(ClientPlant.COLUMN_AMOUNT, amount);
        cv.put(ClientPlant.COLUMN_STATUS, 0);
        cv.put(ClientPlant.COLUMN_REMARK, remarks);
        cv.put(ClientPlant.COLUMN_CLIENTID, clientId);
        cv.put(ClientPlant.COLUMN_PLANTID, plantId);
        long insert = db.insert(ClientPlant.TABLE_NAME, null , cv);
        return insert >= 0 ? true : false;
    }

    // -------------------------CLIENTS----------------------//

    public boolean AddClient(ClientModel model){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(Client.COLUMN_NAME, model.Name);
        cv.put(Client.COLUMN_ADDRESS, model.Address);
        cv.put(Client.COLUMN_PHONENUMBER, model.PhoneNumber);

        long insert = db.insert(Client.TABLE_NAME, null , cv);
        return insert >= 0 ? true : false;
    }
    public void EditClient(ClientModel model){
        SQLiteDatabase db = this.getWritableDatabase();
        String strSql = "UPDATE " + Client.TABLE_NAME + " SET " +
                 Client.COLUMN_NAME + " = " + "'" +model.Name + "'" + " , " +
                 Client.COLUMN_ADDRESS + " = " + "'" + model.Address + "'" + " , " +
                 Client.COLUMN_PHONENUMBER + " = " + "'" + model.PhoneNumber + "'" +
                 " WHERE Id = " + Integer.toString(model.Id);

        db.execSQL(strSql);
        db.close();
    }
    public List<ClientModel> GetClients(){
        List<ClientModel> returnList = new ArrayList<ClientModel>();
        String q = "SELECT * FROM " + Client.TABLE_NAME + " ORDER BY Name";
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(q,null);
        if ( cursor.moveToFirst()){

            do {
                ClientModel client = new ClientModel();
                client.Id = cursor.getInt(0);
                client.Name = cursor.getString(1);
                client.Address = cursor.getString(2);
                client.PhoneNumber = cursor.getString(3);
                returnList.add(client);
            } while(cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return returnList;
    }

    // -------------------------CLIENTS----------------------//
}
// -----------------******--HELPER METHODS --******---------------------//
